import { Component, ElementRef, ViewChild, AfterViewInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';

interface MarcadorColor {
  color: string;
  marker?: mapboxgl.Marker,
  center?: mapboxgl.LngLat
}

@Component({
  selector: 'app-marcadores',
  templateUrl: './marcadores.component.html',
  styles: [`
    .mapa-container {
      width: 100%;
      height: 100vh;
    }

    .list-group {
      position: fixed;
      top: 20px;
      right: 20px;
      z-index: 99999;
    }

    li {
      cursor: pointer;
    }
  `
  ]
})
export class MarcadoresComponent implements AfterViewInit {
  @ViewChild('mapa') divMapa!: ElementRef;
  mapa!: mapboxgl.Map;
  zoomLevel: number = 16;
  center: mapboxgl.LngLat = new mapboxgl.LngLat(-89.70873332208973, 13.737004506112706);

  //Arreglo de marcadores
  marcadores: MarcadorColor[] = [];

  ngAfterViewInit() {
    this.mapa = new mapboxgl.Map({
      container: this.divMapa.nativeElement, // container ID
      style: 'mapbox://styles/mapbox/streets-v12', // style URL
      center: this.center, // starting position [lng, lat]
      zoom: this.zoomLevel, // starting zoom
    });

    this.leerLocalStorage();

    // const marker = new mapboxgl.Marker({
    //   color: "#000",
    //   draggable: true
    // }).setLngLat(this.center)
    //   .addTo(this.mapa);
  }

  agregarMarcador() {
    const color = "#xxxxxx".replace(/x/g, y=>(Math.random()*16|0).toString(16));
    const nuevoMarker = new mapboxgl.Marker({
      color: color,
      draggable: true
    }).setLngLat(this.center)
      .addTo(this.mapa);
    this.marcadores.push({
      color: color,
      marker: nuevoMarker
    });
    this.guardarMarkerLocalStorage();
    //Refresca la data de los marcadores en el localStorage cuando se termina de mover un marker
    nuevoMarker.on('dragend', () => {
      this.guardarMarkerLocalStorage();
    });
  }

  irMarcador(marker: mapboxgl.Marker) {
    this.mapa.flyTo({
      center: marker!.getLngLat()
    });
  }

  guardarMarkerLocalStorage() {
    const lngLatArr = this.marcadores.map(({ color, marker }) => {
      return { color, center: marker?.getLngLat() };
    });
    localStorage.setItem('marcadores', JSON.stringify(lngLatArr))
  }

  leerLocalStorage() {
    if (localStorage.getItem('marcadores')) {
      const lngLatArr: MarcadorColor[] = JSON.parse(localStorage.getItem('marcadores')!);
      lngLatArr.forEach(m => {
        const newMarker = new mapboxgl.Marker({
          color: m.color,
          draggable: true
        }).setLngLat(m.center!)
          .addTo(this.mapa);
        
        this.marcadores.push({
          marker: newMarker,
          color: m.color
        });

        //Refresca la data de los marcadores en el localStorage cuando se termina de mover un marker
        newMarker.on('dragend', () => {
          this.guardarMarkerLocalStorage();
        });
      });
    }
  }

  borrarMarcador(i: number) {
    this.marcadores[i].marker?.remove();
    this.marcadores.splice(i, 1);
    this.guardarMarkerLocalStorage();
  }
}
