import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-zoom-range',
  templateUrl: './zoom-range.component.html',
  styles: [`
    .mapa-container {
      width: 100%;
      height: 100vh;
    }

    .row {
      background-color: white;
      position: fixed;
      bottom: 50px;
      left: 50px;
      padding: 10px;
      border-radius: 5px;
      z-index: 99999;
      width: 400px;
    }
  `
  ]
})
export class ZoomRangeComponent implements AfterViewInit, OnDestroy {

  @ViewChild('mapa') divMapa!: ElementRef;
  mapa!: mapboxgl.Map;
  zoomLevel: number = 16;
  center: mapboxgl.LngLat = new mapboxgl.LngLat(-89.70873332208973, 13.737004506112706);

  ngOnDestroy(): void {
    //Es recomendable siempre borrar los eventos ya que si fuera la misma instancia del elemento cada vez que se navegue a la página,
    //los eventos se estarían duplicando hasta generar una deficiencia,con off solucionamos ese problema
    this.mapa.off('zoom', () => {});
    this.mapa.off('zoomend', () => {});
    this.mapa.off('move', () => {});
  }

  ngAfterViewInit(): void {
    this.mapa = new mapboxgl.Map({
      container: this.divMapa.nativeElement, // container ID
      style: 'mapbox://styles/mapbox/streets-v12', // style URL
      center: this.center, // starting position [lng, lat]
      zoom: this.zoomLevel, // starting zoom
    });

    this.mapa.on('zoom', (ev) => {
      //capturamos el zoom aca ya que al hacerlo dentro de los métodos de zoomIn y zoomOut no se logra obtener con exactitud el zoom
      //ya que por la transición que se realiza se demora unas milésimas de segundo en finalizar y quedar completo
      const zoomActual = this.mapa.getZoom();
      this.zoomLevel = zoomActual;
    });

    this.mapa.on('zoomend', () => {
      if (this.mapa.getZoom() > 18)
        this.mapa.zoomTo(18);
    });

    //Movimiento del mapa
    this.mapa.on('move', (event) => {
      const target = event.target;
      const { lng, lat } = target.getCenter();
      this.center = new mapboxgl.LngLat(lng, lat);
    });
  }

  zoomIn() {
    this.mapa.zoomIn();
  }

  zoomOut() {
    this.mapa.zoomOut();
  }

  zoomCambio(valor: string) {
    //Para establecer el zoom según el valor del input range
    this.mapa.zoomTo(Number(valor));
  }
}
