import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-full-screen',
  templateUrl: './full-screen.component.html',
  styles: [`
    .mapa-container {
      width: 100%;
      height: 100vh;
    }
  `
  ]
})
export class FullScreenComponent implements AfterViewInit {

  @ViewChild('mapa') divMapa!: ElementRef;

  ngAfterViewInit(): void {
    const map = new mapboxgl.Map({
      container: this.divMapa.nativeElement, // container ID
      style: 'mapbox://styles/mapbox/streets-v12', // style URL
      center: [-89.70873332208973, 13.737004506112706], // starting position [lng, lat]
      zoom: 16, // starting zoom
    });
  }
}
